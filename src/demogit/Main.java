package demogit;

public class Main {
	public static void main(String[] args) {
		addition(6, 3);
		subtraction(8,3);
	}
	
	private static void addition(int a, int b) {
		int c = a + b;
		System.out.println(a + " cộng " + b + " bằng " + c);
	}
	
	private static void subtraction(int a, int b) {
		int c = a - b;
		System.out.println(a + " trừ " + b + " bằng " + c);
	}
}